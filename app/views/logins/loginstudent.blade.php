
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Asset('bracket/images/favicon.png') ?>" type="image/png">

    <title>JAKPAT App</title>

    <link href="<?php echo Asset('bracket/css/style.default.css') ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Asset('bracket/js/html5shiv.js') ?>"></script>
    <script src="<?php echo Asset('bracket/js/respond.min.js') ?>"></script>
    <![endif]-->
</head>

<body class="signin">

<!-- Preloader -->
<!--<div id="preloader">-->
<!--    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>-->
<!--</div>-->
<?php $member=new Member(); ?>
<section>

    <div class="signinpanel">

        <div class="row">
            <div></div>
            <div class="col-sm-3 sembunyi">
                
            </div><!-- col-sm-3 -->
            <div class="col-sm-4">
                <img src="bracket/css/images/slogan.png"/>
                <img src="bracket/css/images/tambahan.png"/>
                <img class="subslogan" src="bracket/css/images/subslogan.png">
            </div><!--col-sm-4-->
            <div class="col-sm-5">
                
                @if($member->isError())
                <div class="alert alert-danger">
                    {{ $member->getErrorString() }}
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message }}
                </div>
                @endif
                @if ($message = Session::get('failed'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                @endif
                <h4>For Student</h4>
                {{ Form::model($member,array('route'=>array('login.index'),
                'name'=>'login-form')) }}
                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>
                    {{ Form::text('member[username]',$member->username,array('class'=>'form-control uname','placeholder'=>'Username')) }}
                    {{ Form::password('member[password]',array('class'=>'form-control pword','placeholder'=>'Password')) }}
                    <a href="{{ action('LoginsController@getForgotPassword') }}"><small>Forgot Your Password?</small></a>
                    <button class="btn btn-primary btn-block">Sign In</button>
                {{ Form::close() }}
                <div class="inline-headers">
                    <h5>First time to create survey?</h5>
                    <h4><a href="register">Sign up</a></h4>
                </div><!--inline headers-->
            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2014. All Rights Reserved. JAKPAT App.
            </div>
            <div class="pull-right">
                Created By: JAKPAT App
            </div>
        </div>

    </div><!-- signin -->

</section>


<script src="<?php echo Asset('bracket/js/jquery-1.10.2.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/modernizr.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/retina.min.js') ?>"></script>

</body>
</html>
