<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="{{ asset('bracket/images/favicon.png') }}" type="image/png">

  <title>JAJAK PENDAPAT - Company Registration</title>

  <link href="<?php echo Asset("bracket/css/style.default.css") ?>" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo Asset("bracket/js/html5shiv.js") ?>"></script>
  <script src="<?php echo Asset("bracket/js/respond.min.js") ?>"></script>
  <![endif]-->
</head>

<body class="signin">

<section>
  
    <div class="signuppanel">
        
        <div class="row">
            
            {{ View::make('logins.left_view_registerid') }}
            <div class="col-sm-6" id=''>
            {{ Form::model($member,array("route"=>array("register"), "name"=>"register-form",'id'=>'register-form')) }}
            <div class="" id='signup-form'>
            <h3 class="nomargin">Sign Up {{ $type=='student'?'Untuk Siswa / Mahasiswa':'' }} </h3>
                <p class="mt5 mb20">Sudah menjadi member? <a href="{{ URL::to($type=='student'?'idn/login/student':'idn/login') }}">Sign In</a></p>

                    <div class="mb10">
                        <label class="control-label">Nama Depan</label>
                        {{ Form::text(
                            "first_name",
                            Input::old("first_name"),
                            array(
                                "class" => "form-control required"
                            ))
                        }}
                        <span class="err"> {{$errors->first("first_name")}} </span>
                    </div>
                    <div class="mb10">
                        <label class="control-label">Nama Belakang</label>
                        {{ Form::text(
                            "last_name",
                            Input::old("last_name"),
                            array(
                                "class" => "form-control  required"
                            ))
                        }}
                        <span class="err"> {{$errors->first("last_name")}} </span>
                    </div>
                    
                    <div class="mb10">
                        <label class="control-label">Username</label>
                        {{ Form::text(
                            "username",
                            Input::old("username"),
                            array(
                                "class" => "form-control  required"
                            ))
                        }}
                        <span class="err"> {{$errors->first("username")}} </span>
                    </div>
                    
                    <div class="mb10">
                        <label class="control-label">Password</label>
                        {{ Form::password(
                            "password",
                            array(
                                "class" => "form-control  required",'minlength'=>6,
                                "id"=>"password"
                            ))
                        }}
                        <span class="err"> {{$errors->first("password")}} </span>
                    </div>
                    
                    <div class="mb10">
                        <label class="control-label">Ketik Ulang Password</label>
                        {{ Form::password(
                            "retype_password",
                            array(
                                "class" => "form-control",
                                
                            ))
                        }}
                        <span class="err"> {{$errors->first("retype_password")}} </span>
                    </div>
                    
                    <div class="mb10">
                        <label class="control-label">Email</label>
                        {{ Form::text(
                            "email",
                            Input::old("email"),
                            array(
                                "class" => "form-control  required email",

                            ))
                        }}
                        <span class="err"> {{$errors->first("email")}} </span>
                    </div>

                    <br />
                    
                    <a class="btn btn-primary btn-block" id="next-btn">Next</a>     
                </div><!-- col-sm-6 -->

            @if(isset($type) && $type=='student')
                {{ Form::hidden("type",'student',array("class" => "form-control required")) }}
                {{ View::make('logins.student_formid') }}        
            @else
                {{ Form::hidden("type",'company',array("class" => "form-control required")) }}
                {{ View::make('logins.company_formid') }}
            @endif
            </form>
            </div><!-- col-sm-6 -->
        </div><!-- row -->
        
        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2014. All Rights Reserved. JAKPAT App
            </div>
            <div class="pull-right">
                Created By: JAKPAT App
            </div>
        </div>
        
    </div><!-- signuppanel -->
  
</section>

<script src="<?php echo Asset("bracket/js/jquery-1.10.2.min.js") ?>"></script>
<script src="<?php echo Asset("bracket/js/jquery-migrate-1.2.1.min.js") ?>"></script>
<script src="<?php echo Asset("bracket/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo Asset("bracket/js/modernizr.min.js") ?>"></script>
<script src="<?php echo Asset("bracket/js/retina.min.js") ?>"></script>
<script src="<?php echo Asset('bracket/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#next-btn').click(function(e){
            var isValid=true;
            var formList=$('#signup-form').find('input');
            for(var i=0;i<formList.length;i++){
               isValid=isValid && $(formList[i]).valid();
            }
            if(isValid){
                $('#signup-form').hide();
                $('#information-form').show();
            }
        });
        $("#register-form").validate({
            rules: {
              retype_password: { 
                    equalTo: "#password"
              },
              username:{
                    remote:"{{ URL::to("login/validate/username") }}"
              },
              email:{
                    remote:"{{ URL::to("login/validate/email") }}"
              }
            },
            messages:{
                username:{remote:"username already used by another user"},
                email:{remote:"email already used by another user"}
            } 
          });
    })
</script>
</body>
</html>
