
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Asset('bracket/images/favicon.png') ?>" type="image/png">

    <title>JakPat</title>

    <link href="<?php echo Asset('bracket/css/style.default.css') ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Asset('bracket/js/html5shiv.js') ?>"></script>
    <script src="<?php echo Asset('bracket/js/respond.min.js') ?>"></script>
    <![endif]-->
</head>

<body class="signin">

<!-- Preloader -->
<!--<div id="preloader">-->
<!--    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>-->
<!--</div>-->

<section>

    <div class="signinpanel">

        <div class="row">
            <div></div>
            <div class="col-md-7">
                @if($member->isError())
                <div class="alert alert-danger">
                    {{ $member->getErrorString() }}
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message }}
                </div>
                @endif
                @if ($message = Session::get('failed'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                @endif
                <div class="signinpanel" style="margin-top: 0px;">
                    <div></div>
                    <div class="col-sm-3 sembunyi">
                        <img class="logo" src="{{ Asset('bracket/css/images/jackpat_phone.png') }}"/>
                    </div><!-- col-sm-3 -->
                    <div class="col-sm-4">
                        <img src="{{ Asset('bracket/css/images/slogan.png') }}"/>
                        <img class="subslogan" src="{{ Asset('bracket/css/images/subslogan.png') }}">
                    </div><!--col-sm-4-->

                    <div class="mb20"></div>
                    
                </div><!-- signin0-info -->
            </div><!-- col-sm-7 -->

            <div class="col-md-5">
                {{ Form::model($member,array('route'=>array('resetPassword'),
                'name'=>'login-form')) }}
                    <h4 class="nomargin">Reset Password</h4>
                    {{ Form::hidden('email') }}
                    <span class="err"> {{$errors->first('email')}} </span>
                    {{ Form::hidden('verify_key') }}
                    <span class="err"> {{$errors->first('verify_key')}} </span>
                    {{ Form::password('password',array('class'=>'form-control pword','placeholder'=>'New Password')) }}
                    <span class="err"> {{$errors->first('password')}} </span>
                    {{ Form::password('retype_password',array('class'=>'form-control pword','placeholder'=>'Retype New Password')) }}
                    <span class="err"> {{$errors->first('retype_password')}} </span>
                    <button class="btn btn-success btn-block">Save</button>
                    <strong>Not a member? <a href="{{route('register')}}">Sign Up</a></strong>
                {{ Form::close() }}
            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2014. All Rights Reserved. Bracket Bootstrap 3 Admin Template
            </div>
            <div class="pull-right">
                Created By: <a href="http://themepixels.com/" target="_blank">ThemePixels</a>
            </div>
        </div>

    </div><!-- signin -->

</section>


<script src="<?php echo Asset('bracket/js/jquery-1.10.2.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/modernizr.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/retina.min.js') ?>"></script>

</body>
</html>
