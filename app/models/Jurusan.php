<?php

class Jurusan extends \MyModel {
	public $fillable = [];
    protected $collection='jurusan';

    public function validator($data){
        $id=null;
        if(!$this->isNewRecord()){
            $id=",".$this->{$this->primaryKey}.','.$this->primaryKey;
        }
        $rules = [
            'nama' => 'required',
            'kode_jurusan'=>'required|max:10'
        ];
        return Validator::make($data, $rules);
    }
    public static function gridSort($columnInt,$orderType)
    {
        switch($columnInt)
        {
            case 1: $column="jurusan";
            break;
            case 2 :$column="kode_jurusan";
            break;
        }
        $query=Jurusan::whereRaw('1');
        $searchList=Input::get('jurusan',array());
        foreach($searchList as $key=>$search){
            if($search==null) continue;
            $query->where($key,'like',"%$search%");
        }
        // $query->explain();
        // $query->sort($column,$orderType);
        // $result=$query->paginate();
        unset($_GET['page']);
        // $result->appends($_GET);
        return Jurusan::all();
    }

}