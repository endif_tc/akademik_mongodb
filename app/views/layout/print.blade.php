<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="shortcut icon" href="{{ asset('bracket/images/favicon.png') }}" type="image/png">
 -->
  <title>Ariang Trans</title>
  <link href="{{ Asset('bracket/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/customize.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.datatables.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/CreditCardTypeDetector/creditCardTypeDetector.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/angucomplete.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/kursi.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/colorpicker.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.ui-datepicker.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.gritter.css') }}" rel="stylesheet">

  <style type="text/css" >
    .NonPrintable
    {
      display: none;
    }
    body{
      background: #fff;
    }
    th.center{
      text-align: center;
    }
    h3.title,h4.title,h5.title{
      text-align: center;     
    }
  </style>
</head>

<body style="overflow: visible;">
    <div class="contentpanel" style="margin:30px;">
        {{ View::make($view,$params)->render() }}
    </div>
</body>
</html>
