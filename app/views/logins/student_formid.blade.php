<div class="" id='information-form' style='display:none'>
        <h3 class="nomargin">Sign Up Untuk Siswa / Mahasiswa</h3>
        <p class="mt5 mb20">Sudah menjadi member? <a href="{{ URL::to("idn/login/student") }}"><strong>Sign In</strong></a></p>

        <div class="mb10">
            <label class="control-label">Nama Universitas / Sekolah</label>
            {{ Form::text(
                "university",
                Input::old("university"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("university")}} </span>
        </div>
        <div class="mb10">
            <label class="control-label">Website Universitas / Sekolah (Jika ada)</label>
            {{ Form::text(
                "university_website",
                Input::old("university_website"),
                array(
                    "class" => "form-control url"
                ))
            }}
            <span class="err"> {{$errors->first("university_website")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Lokasi Universitas / Sekolah (Kota)</label>
            {{ Form::text(
                "university_location",
                Input::old("university_location"),
                array(
                    "class" => "form-control"
                ))
            }}
            <span class="err"> {{$errors->first("university_location")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Nomor Kartu Tanda Mahasiswa / Pelajar</label>
            {{ Form::text(
                "card_number",
                Input::old("card_number"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("card_number")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Jurusan</label>
            {{ Form::text(
                "university_major",
                Input::old("university_major"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("university_major")}} </span>
        </div>
        
        <br />
        
        <button class="btn btn-primary btn-block">Sign Up</button>     
    </form>
</div><!-- col-sm-6 -->