<?php

class MahasiswaController extends \BaseController {
	public function __construct()
    {
    	parent::__construct();
		$this->pageheader = '<i class="glyphicon glyphicon-globe"></i> Data Mahasiswa';
    }
	/**
	 * Display a listing of mahasiswa
	 *
	 * @return Response
	 */
	public function index()
	{
		$columnInt=\Input::get('sort',1);
		$orderType=\Input::get('orderType',"asc");
		$mahasiswaList = Mahasiswa::gridSort($columnInt,$orderType);
		return $this->render('mahasiswa.index', compact('mahasiswaList','columnInt','orderType'));
	}
	/**
	 * Show the form for creating a new mahasiswa
	 *
	 * @return Response
	 */
	public function create()
	{
		$mahasiswa=new Mahasiswa();
		$jurusanList=Jurusan::all();
		// Helper::dropDownModel($jurusanList,'_id','nama');exit;
		// Helper::show_array($jurusanList->toArray());exit;
		return $this->render('mahasiswa.form',compact('mahasiswa','jurusanList'));
	}

	/**
	 * Store a newly created mahasiswa in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data=Input::get('mahasiswa');
        $mahasiswa=new Mahasiswa();
       	$mahasiswa->setRawAttributes($data);
		$validator = $mahasiswa->validator($data);
		if ($validator->fails())
		{
			return Redirect::route('mahasiswa.create')->withErrors($validator)->withInput();
		}
		$mahasiswa=new Mahasiswa();
		$mahasiswa->setRawAttributes($data);
       	// $mahasiswa->id_jurusan=Jurusan::first($data['id_jurusan']);
        $act=$mahasiswa->save();
        $mahasiswa->setRawAttributes($data);
        $this->message($act,'Data mahasiswa <b>'.$mahasiswa->mahasiswa.'</b> berhasil ditambahkan','Data mahasiswa <b>'.$mahasiswa->mahasiswa.'</b> gagal ditambahkan');
        return Redirect::route('mahasiswa.index');
	}

	/**
	 * Display the specified mahasiswa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$mahasiswa = Mahasiswa::findOrFail($id);

		return $this->render('mahasiswa.show', compact('mahasiswa'));
	}

	/**
	 * Show the form for editing the specified mahasiswa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mahasiswa = Mahasiswa::find($id);
		return $this->render('mahasiswa.edit', compact('mahasiswa'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$mahasiswa = Mahasiswa::first($id);
		// Helper::show_array($mahasiswa);exit;
		$data=Input::get('mahasiswa');
		// Helper::show_array($data);exit;

		$validator = $mahasiswa->validator($data);
		if ($validator->fails())
		{
			return Redirect::route('mahasiswa.edit',array($id))->withErrors($validator)->withInput();
		}else{
			$mahasiswa->setRawAttributes($data);
		// Helper::show_array($mahasiswa);exit;

			// echo $mahasiswa->_id.'<br>';
            $is_success=$mahasiswa->save();
			// echo $mahasiswa->_id;
			// exit;
            $this->message($is_success,'Mahasiswa <b>'.$mahasiswa->nama.'</b> berhasil diedit','Mahasiswa '.$mahasiswa->mahasiswa.' gagal diedit');
            if($is_success){
                return Redirect::intended('mahasiswa');
            }
		}

		// $mahasiswa->update($data);

		return Redirect::route('mahasiswa.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
    	$mahasiswa=Mahasiswa::find($id);
        try{
            $isSuccess=$mahasiswa->delete();
        }catch (Exception $e){
        	$isSuccess=false;
        }
        $this->message($isSuccess,"Mahasiswa <b>$mahasiswa->nama</b> berhasil dihapus","Mahasiswa <b>$mahasiswa->mahasiswa</b> gagal dihapus. Kemungkinan sudah digunakan untuk manifest");
        return Redirect::intended('mahasiswa');
    }
}
