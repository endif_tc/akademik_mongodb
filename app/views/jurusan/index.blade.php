@extends('layout/row')
@section('content')
<?php
$jurusan = new Jurusan();
$jurusan->setRawAttributes(Input::get('jurusan', array()));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="#" class="panel-close" data-dismiss="modal">&times;</a>
            </div>
            <h4 class="panel-title">Jurusan</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="row-fluid">
                <div class="col-sm-6" style="text-align: left">
                    <div class="cms-admin-buttons">
                        <a class="btn btn-default" target='ajax-modal' href="<?php echo URL::route('jurusan.create') ?>">
                            <i class="glyphicon-plus glyphicon"></i> 
                            Tambah Jurusan
                        </a>
                    </div>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <form id="yw0" action="{{ URL::route('jurusan.index') }}" method="get">
                        <div class="input-group mb10">
                            <input class="col-sm-3 form-control" placeholder="Nama Jurusan" name="jurusan[jurusan]"
                                   type="text" maxlength="50" value="{{ $jurusan->jurusan }}" style="margin-top: 1px;">

                            <div class="input-group input-group-btn">
                                <button data-dismiss="modal" onclick="" class="btn btn-primary"
                                        type="submit"><i class="glyphicon-search glyphicon"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table mb30 display dataTable table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th><a style="text-decoration:none" href="{{ URL::to('jurusan?sort=1&orderType='.($columnInt==1 && $orderType=='asc'?'desc':'asc').'&'.Helper::generateGetParam('jurusan')) }}">Jurusan</a></th>
                        <th><a style="text-decoration:none" href="{{ URL::to('jurusan?sort=2&orderType='.($columnInt==2 && $orderType=='asc'?'desc':'asc').'&'.Helper::generateGetParam('jurusan')) }}">Kode Jurusan</a></th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($jurusanList as $jurusan)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $jurusan->nama }}</td>
                        <td>{{ $jurusan->kode_jurusan }}</td>
                        <td class="table-action">
                            <div class="btn-group">
                                <a href="{{ URL::route('jurusan.edit',$jurusan->_id) }}" title="Edit Jurusan" class='btn btn-primary-alt btn-xs' target='ajax-modal'>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {{ Form::model($jurusan,array('route'=>array('jurusan.destroy',$jurusan->_id),
                                    'method'=>'DELETE','class'=>'form-inline')) }}
                                <button class='btn btn-primary-alt btn-xs trash' 
                                   onclick="return confirmDeleteDialog('Apakah anda yakin akan menghapus jurusan <b>{{ $jurusan->nama }}</b>?',this)" title="Delete Jurusan">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                 {{ Form::close() }}
                             </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <?php //echo with(new OveyPaginator($jurusanList))->render(); ?>
            </ul>
        </div>
    </div>
</div>

@stop