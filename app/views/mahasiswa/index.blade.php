@extends('layout/row')
@section('content')
<?php
$mahasiswa = new Mahasiswa();
$mahasiswa->setRawAttributes(Input::get('mahasiswa', array()));
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="#" class="panel-close" data-dismiss="modal">&times;</a>
            </div>
            <h4 class="panel-title">Mahasiswa</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="row-fluid">
                <div class="col-sm-6" style="text-align: left">
                    <div class="cms-admin-buttons">
                        <a class="btn btn-default" target='ajax-modal' href="<?php echo URL::route('mahasiswa.create') ?>">
                            <i class="glyphicon-plus glyphicon"></i> 
                            Tambah Mahasiswa
                        </a>
                    </div>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <form id="yw0" action="{{ URL::route('mahasiswa.index') }}" method="get">
                        <div class="input-group mb10">
                            <input class="col-sm-3 form-control" placeholder="Nama Mahasiswa" name="mahasiswa[mahasiswa]"
                                   type="text" maxlength="50" value="{{ $mahasiswa->mahasiswa }}" style="margin-top: 1px;">

                            <div class="input-group input-group-btn">
                                <button data-dismiss="modal" onclick="" class="btn btn-primary"
                                        type="submit"><i class="glyphicon-search glyphicon"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table mb30 display dataTable table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th><a style="text-decoration:none" href="{{ URL::to('mahasiswa?sort=1&orderType='.($columnInt==1 && $orderType=='asc'?'desc':'asc').'&'.Helper::generateGetParam('mahasiswa')) }}">NIM</a></th>
                        <th><a style="text-decoration:none" href="{{ URL::to('mahasiswa?sort=2&orderType='.($columnInt==2 && $orderType=='asc'?'desc':'asc').'&'.Helper::generateGetParam('mahasiswa')) }}">Nama</a></th>
                        <th><a style="text-decoration:none" href="{{ URL::to('mahasiswa?sort=2&orderType='.($columnInt==3 && $orderType=='asc'?'desc':'asc').'&'.Helper::generateGetParam('mahasiswa')) }}">Jurusan</a></th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($mahasiswaList as $mahasiswa)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $mahasiswa->nim }}</td>
                        <td>{{ $mahasiswa->nama }}</td>
                        <td>{{ $mahasiswa->jurusan()->nama }}</td>
                        <td class="table-action">
                            <div class="btn-group">
                                <a href="{{ URL::route('mahasiswa.edit',$mahasiswa->_id) }}" title="Edit Mahasiswa" class='btn btn-primary-alt btn-xs' target='ajax-modal'>
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {{ Form::model($mahasiswa,array('route'=>array('mahasiswa.destroy',$mahasiswa->_id),
                                    'method'=>'DELETE','class'=>'form-inline')) }}
                                <button class='btn btn-primary-alt btn-xs trash' 
                                   onclick="return confirmDeleteDialog('Apakah anda yakin akan menghapus mahasiswa <b>{{ $mahasiswa->nama }}</b>?',this)" title="Delete Mahasiswa">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                 {{ Form::close() }}
                             </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <?php //echo with(new OveyPaginator($mahasiswaList))->render(); ?>
            </ul>
        </div>
    </div>
</div>

@stop