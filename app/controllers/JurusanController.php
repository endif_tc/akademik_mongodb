<?php

class JurusanController extends \BaseController {
	public function __construct()
    {
    	parent::__construct();
		$this->pageheader = '<i class="glyphicon glyphicon-globe"></i> Data Jurusan';
    }
	/**
	 * Display a listing of jurusan
	 *
	 * @return Response
	 */
	public function index()
	{
		$columnInt=\Input::get('sort',1);
		$orderType=\Input::get('orderType',"asc");
		$jurusanList = Jurusan::gridSort($columnInt,$orderType);
		return $this->render('jurusan.index', compact('jurusanList','columnInt','orderType'));
	}
	/**
	 * Show the form for creating a new jurusan
	 *
	 * @return Response
	 */
	public function create()
	{
		$jurusan=new Jurusan();
		return $this->render('jurusan.form',array('jurusan'=>$jurusan));
	}

	/**
	 * Store a newly created jurusan in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data=Input::get('jurusan');
        $jurusan=new Jurusan();
       	$jurusan->setRawAttributes($data);
		$validator = $jurusan->validator($data);
		if ($validator->fails())
		{
			return Redirect::route('jurusan.create')->withErrors($validator)->withInput();
		}
		$jurusan=new Jurusan();
		$jurusan->setRawAttributes($data);
        $act=$jurusan->save();
        $jurusan->setRawAttributes($data);
        $this->message($act,'Data jurusan <b>'.$jurusan->jurusan.'</b> berhasil ditambahkan','Data jurusan <b>'.$jurusan->jurusan.'</b> gagal ditambahkan');
        return Redirect::route('jurusan.index');
	}

	/**
	 * Display the specified jurusan.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$jurusan = Jurusan::findOrFail($id);

		return $this->render('jurusan.show', compact('jurusan'));
	}

	/**
	 * Show the form for editing the specified jurusan.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$jurusan = Jurusan::find($id);
		return $this->render('jurusan.edit', compact('jurusan'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$jurusan = Jurusan::first($id);
		// Helper::show_array($jurusan);exit;
		$data=Input::get('jurusan');
		// Helper::show_array($data);exit;

		$validator = $jurusan->validator($data);
		if ($validator->fails())
		{
			return Redirect::route('jurusan.edit',array($id))->withErrors($validator)->withInput();
		}else{
			$jurusan->setRawAttributes($data);
		// Helper::show_array($jurusan);exit;

			// echo $jurusan->_id.'<br>';
            $is_success=$jurusan->save();
			// echo $jurusan->_id;
			// exit;
            $this->message($is_success,'Jurusan <b>'.$jurusan->nama.'</b> berhasil diedit','Jurusan '.$jurusan->jurusan.' gagal diedit');
            if($is_success){
                return Redirect::intended('jurusan');
            }
		}

		// $jurusan->update($data);

		return Redirect::route('jurusan.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
    	$jurusan=Jurusan::find($id);
        try{
            $isSuccess=$jurusan->delete();
        }catch (Exception $e){
        	$isSuccess=false;
        }
        $this->message($isSuccess,"Jurusan <b>$jurusan->nama</b> berhasil dihapus","Jurusan <b>$jurusan->jurusan</b> gagal dihapus. Kemungkinan sudah digunakan untuk manifest");
        return Redirect::intended('jurusan');
    }
}
