<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="shortcut icon" href="{{ asset('bracket/images/favicon.png') }}" type="image/png">
 -->
  <title>Ariang Trans</title>
<link href="{{ Asset('bracket/css/style.default.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/customize.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.datatables.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/CreditCardTypeDetector/creditCardTypeDetector.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/angucomplete.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/kursi.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/colorpicker.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.ui-datepicker.css') }}" rel="stylesheet">
  <link href="{{ Asset('bracket/css/jquery.gritter.css') }}" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="{{ Asset('bracket/js/html5shiv.js') }}"></script>
  <script src="{{ Asset('bracket/js/respond.min.js') }}"></script>
  <![endif]-->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
-->
  <script src="{{ Asset('bracket/js/jquery-1.10.2.min.js') }}"></script>
  <script src="{{ Asset('bracket/js/jquery-ui-1.10.3.min.js') }}"></script>
  <script src="{{ Asset('bracket/js/bootbox.min.js') }}"></script>
  <script src="{{ Asset('bracket/js/jquery.gritter.min.js') }}"></script>

  <script src="{{ Asset('bracket/js/angular.1.2.16.min.js') }}"></script>
  <script src="{{ Asset('bracket/js/elastic.js') }}"></script>

  <script src="{{ Asset('bracket/js/bootstrap-wizard.min.js') }}"></script>
  <script src="{{ Asset('bracket/js/accounting.js') }}"></script>
  <script src="{{ Asset('bracket/CreditCardTypeDetector/jquery.creditCardTypeDetector.js') }}"></script>

  <script src="{{ Asset('bracket/js/angular-validation.js')}}"></script>
  <script src="{{ Asset('bracket/js/angular-validation-rules.js')}}"></script>

  <link href="{{ Asset('bracket/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">

  <style type="text/css" media="print">
    .NonPrintable
    {
      display: none;
    }
  </style>

</head>

<body style="overflow: visible;">

<!-- Preloader -->
<!--<div id="preloader">-->
<!--    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>-->
<!--</div>-->

<section>

  <div class="leftpanel">
<!--
    <div class="logopanel">
        <img src="{{ Asset('bracket/css/images/minilogo.png') }}" style="width: 130px;" />
    </div> -->

    <div class="leftpanelinner">
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media userlogged">
                <img alt="" src="{{ Asset('bracket/images/photos/loggeduser.png') }}" class="media-object">
                <div class="media-body">
                    <h4>{{ ucwords(Auth::user()->first_name) }} {{ ucwords(Auth::user()->last_name) }}</h4>
                </div>
            </div>

            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
              <li><a href="{{ URL::to('logout') }}"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>

      <h5 class="sidebartitle">Navigation</h5>
      {{ View::make('layout/menu',compact('user'))->render() }}

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

    <div class="headerbar">

      <a class="menutoggle"><i class="fa fa-bars"></i></a>
      <!--
      <form class="searchform" action="http://themepixels.com/demo/webpage/bracket/index.html" method="post">
        <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
      </form>
      -->
      <div class="header-right">
        <ul class="headermenu">
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                {{ ucwords(Auth::user()->first_name).' '.ucwords(Auth::user()->last_name) }}
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="{{ URL::to('pengguna/showProfil') }}"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                <li><a href="{{ URL::to('logout') }}"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div><!-- header-right -->

    </div><!-- headerbar -->

    <div class="pageheader">
      <h2>@if(isset($pageheader)) {{$pageheader}} @else <i class="fa fa-home"></i> Dashboard @endif @if(isset($pageheader_desc)) <span>{{$pageheader_desc}}</span> @endif</h2>
      <div class="breadcrumb-wrapper">
        <!--
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Bracket</a></li>
          <li class="active">Dashboard</li>
        </ol>
        -->
      </div>
    </div>

    <div class="contentpanel">
      <div id="alert-container">

            @if(Session::has('errors'))
              <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Helper::validatorMessageStrFromArray($errors = Session::get('errors')->getBag('default')->all()) }}
                </div>
          @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message }}
                </div>
            @endif
            @if ($message = Session::get('failed'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message }}
                </div>
            @endif
            @if($validator!=null)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Helper::validatorMessageStr($validator) }}
                </div>
            @endif
        </div>
        <div class="panel panel-default">
          <div class="modal fade" id="myModelDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          </div>
        </div>
        {{ View::make($view,$params)->render() }}
    </div><!-- contentpanel -->

  </div><!-- mainpanel -->


</section>


<script src="{{ Asset('bracket/js/jquery-migrate-1.2.1.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/bootstrap.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/modernizr.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/jquery.validate.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/localization/messages_id.js'); }}"></script>
<script src="{{ Asset('bracket/js/jquery.sparkline.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/toggles.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/retina.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/jquery.cookies.js'); }}"></script>

<script src="{{ Asset('bracket/js/bootstrap-timepicker.min.js') }}"></script>

<script src="{{ Asset('bracket/js/morris.min.js'); }}"></script>
<script src="{{ Asset('bracket/js/raphael-2.1.0.min.js'); }}"></script>

<script src="{{ Asset('bracket/js/chosen.jquery.min.js'); }}"></script>

<script src="{{ Asset('bracket/js/custom/dialog.js'); }}"></script>
<script src="{{ Asset('bracket/js/custom/library.js'); }}"></script>
<script src="{{ Asset('bracket/js/custom.js'); }}"></script>
<script src="{{ Asset('bracket/js/elastic.js'); }}"></script>



</body>
</html>
