<?php
class MyModel extends MongoLid{
	public $database="akademik";
	public function setRawAttributes(array $attributes, $sync = false)
	{
		$tempId=$this->_id;
		$this->attributes = $attributes;
		if(!$this->isNewRecord())
			$this->_id=$tempId;
		if ($sync) $this->syncOriginal();
	}

	/**
	 * Get the model's original attribute values.
	 *
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return array
	 */
	public function getOriginal($key = null, $default = null)
	{
		return array_get($this->original, $key, $default);
	}

	/**
	 * Sync the original attributes with the current.
	 *
	 * @return $this
	 */
	public function syncOriginal()
	{
		$this->original = $this->attributes;

		return $this;
	}
	public function isNewRecord(){
		// echo $this->_id;exit;
        return !$this->exists && $this->_id==null;
    }
    public function isError(){
        return count($this->errors)>0;
   }
}
?>