$('body').delegate('a[target=ajax-modal],button[target=ajax-modal]','click',function(){
        var url = $(this).attr('href');
        // if(is_mobile()){
        //     $(this).attr('target','');
        //     window.location=url;
        //     return true;
        // }
        ajaxModal(url,$(this));
        return false;
});
$('body').delegate('a[target=print],button[target=print],a.print','click',function(){
        var url = $(this).attr('href');
        window.open(url,'popUpWindow','height=600,width=900,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
        return false;
});
function ajaxModal(url,el){
  $('#myModelDialog').css({'width':''});
  if($(el).hasClass('modal-max')){
//      $('#myModelDialog').addClass('modal-max');
      $('#myModelDialog').css({'width':'900px'});
      console.log('tess');
  }
  
  $('#myModelDialog').html();
  $.ajax({
        url: url,
        data:'ajax=1',
        cache: false,
        dataType: 'html',
        success: function(msg){
            $('#myModelDialog').html(msg);
            $('#myModelDialog').modal('show');
        },
        error: function(){
            $('#myModelDialog').html("request gagal dibuka");
            $('#myModelDialog').modal('show');
            console.log('gagal');
        }
  });
  return true;    
}
function confirmDeleteDialog(dialogText,el){
    bootbox.confirm(dialogText, function(result) {
      if(result){
            $(el).parents('form').submit();
      }
    }); 
    return false;
}

function alertDanger(title,desc){
  jQuery.gritter.add({
    title: title,
    text: desc,
      class_name: 'growl-danger',
      image: 'images/screen.png',
    sticky: false,
    time: ''
   });
}
function alertSuccess(title,desc){
  jQuery.gritter.add({
    title: title,
    text: desc,
      class_name: 'growl-success',
      image: 'images/screen.png',
    sticky: false,
    time: ''
   });
}