<?php

// use Jenssegers\Mongodb\Model as Eloquent;

class Mahasiswa extends MyModel{
	protected $collection="mahasiswa";
	public function validator($data){
        $id=null;
        if(!$this->isNewRecord()){
            $id=",".$this->{$this->primaryKey}.','.$this->primaryKey;
        }
        $rules = [
            'nama' => 'required',
            'nim' => 'required',
            'id_jurusan'=>'required'
        ];
        return Validator::make($data, $rules);
    }
    public static function gridSort($columnInt,$orderType)
    {
        switch($columnInt)
        {
            case 1: $column="jurusan";
            break;
            case 2 :$column="kode_jurusan";
            break;
        }
        $query=Mahasiswa::whereRaw('1');
        $searchList=Input::get('jurusan',array());
        foreach($searchList as $key=>$search){
            if($search==null) continue;
            $query->where($key,'like',"%$search%");
        }
        // $query->explain();
        // $query->sort($column,$orderType);
        // $result=$query->paginate();
        unset($_GET['page']);
        // $result->appends($_GET);
        return Mahasiswa::all();
    }
    public function jurusan()
        {
        	return Jurusan::find($this->id_jurusan);
            return $this->embedsOne('Jurusan', 'id_jurusan');
        }
}