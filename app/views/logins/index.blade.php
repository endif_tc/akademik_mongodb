<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Asset('bracket/images/favicon.png') ?>" type="image/png">

    <title>Ariang Trans</title>

    <link href="<?php echo Asset('bracket/css/style.default.css') ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo Asset('bracket/js/html5shiv.js') ?>"></script>
    <script src="<?php echo Asset('bracket/js/respond.min.js') ?>"></script>
    <![endif]-->
</head>

<body class="signin">

<!-- Preloader -->
<!--<div id="preloader">-->
<!--    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>-->
<!--</div>-->

<section>

    <div class="signinpanel">

        <div class="row">
            <div></div>
            <div class="col-sm-3 sembunyi">
                <img class="logo" src="{{ Asset('bracket/images/bis.png') }}"/>
            </div><!-- col-sm-3 -->
            <div class="col-sm-4">
                &nbsp;
                <br/><br/>
                <img class="subslogan" src="{{ Asset('bracket/css/images/subslogan.png') }}" class="subslogan" />

            </div><!--col-sm-4-->
            <div class="col-sm-5">
                
                @if($user->isError())
                <div class="alert alert-danger">
                    {{ $user->getErrorString() }}
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message }}
                </div>
                @endif
                @if ($message = Session::get('failed'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                @endif
                @if(isset($user_type) && $user_type=='student')
                    <h4 class="nomargin">For Student</h4>
                @endif
                {{ Form::model($user,array('route'=>array('login.index'),
                'name'=>'login-form')) }}
                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>
                    {{ Form::text ('user[username]',$user->username,array('class'=>'form-control uname','placeholder'=>'Username','autofocus'=>'autofocus')) }}
                    {{ Form::password('user[password]',array('class'=>'form-control pword','placeholder'=>'Password')) }}
                    
                    <button class="btn btn-primary btn-block">Sign In</button>
                {{ Form::close() }}
                <div class="inline-headers">
                    <h5>First time to create survey?</h5>
                    <h4><a href='{{ URL::to(isset($user_type) && $user_type=='student'?'register/student':'register') }}'>Sign up</a></h4>
                </div><!--inline headers-->
            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2014. All Rights Reserved.
            </div>
            <div class="pull-right">
                Created By: GgTechno
            </div>
        </div>

    </div><!-- signin -->

</section>

<script src="<?php echo Asset('bracket/js/jquery-1.10.2.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/modernizr.min.js') ?>"></script>
<script src="<?php echo Asset('bracket/js/retina.min.js') ?>"></script>


</body>
</html>
