<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::resource('login', ('LoginsController'));
Route::post('login', 'LoginsController@index');
Route::get('logout', 'LoginsController@logout');

Route::group(array('before' => 'isLogin'), function()
{
Route::get('/', 'HomeController@showWelcome');
Route::resource('jurusan', 'JurusanController');
Route::resource('mahasiswa', 'MahasiswaController');
});

Route::get('initUser',function(){
	$user=new User();
	$user->username='admin';
	$user->password=Hash::make(trim('1234'));
	$user->first_name = trim('Siti');
    $user->last_name = trim('Aminah');
    $user->email = trim('admin@mongo.com');
    $user->save();
    return "tes";
});
