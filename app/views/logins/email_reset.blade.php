<div bgcolor="#ffffff" style="margin:0;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;min-height:100%;width:100%!important">

<table bgcolor="#f3ac00" border="0" style="margin:0;padding:0;width:100%">
  <tbody>
    <tr style="margin:0;padding:0">
      <td></td>
      <td style="margin:0 auto!important;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important">
        <div style="margin:0 auto;padding:7px 15px;max-width:600px;display:block;background:#f3ac00">
          <table bgcolor="#f3ac00" style="margin:0;padding:0;width:100%">
            <tbody>
              <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">
                  
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </td>
      <td></td>
    </tr>
  </tbody>
</table>

<table border="0" style="margin:0;padding:0;width:100%">
  <tbody>
  <tr style="margin:0;padding:0">
    <td></td>
    <td bgcolor="#FFFFFF" style="margin:0 auto!important;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important">
      <div style="margin:0 auto;padding:10px 15px 15px 15px;max-width:600px;display:block;background:#ffffff;line-height:2;color:#65656f"><h1>Hi there, {{ ucwords($name) }}!</h1>
        <p>You or someone has requested to reset the password. If you don't request for a password reset, please ignore this email. </p>
        <a href="{{ $url }}" style="background-color:#4E6EAD;font-size:12px;padding:9px 16px;border:0 none;border-radius:100px 100px 100px 100px;color:white;display:inline-block;font-weight:600;letter-spacing:1px;line-height:1.2;margin:0;outline:medium none;text-align:center;text-decoration:none;text-transform:uppercase;vertical-align:middle;margin-top:10px;margin-bottom:20px" target="_blank">Reset your password</a>
        <p>Or, copy the following URL and paste it in your browser:</p>
        <a href="#" style="color:#aaaaaa;text-decoration:none" target="_blank">{{ $url }}</a>
        <p style="margin-top:30px">Thanks for using JAKPAT!</p>
      </div>
    </td>
    <td></td>
  </tr>
  </tbody>
</table>

<table border="0" style="margin:0;padding:0;width:100%;clear:both!important">
  <tbody>
    <tr style="margin:0;padding:0">
      <td style="margin:0 auto!important;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;display:block!important;max-width:600px!important;clear:both!important">
          <div style="margin:0 auto;padding:15px 15px 45px 15px;max-width:600px;display:block;background:#ffffff;text-align:left">
            <p style="margin:0;padding:0;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:gray;border-top:1px solid #cccccc;padding-top:15px">
              This email was sent to <a href="mailto:{{ $email }}" style="color:gray" target="_blank">{{ $email }}</a>
              by JAKPAT GIT.              
            </p>
        </div>
      </td>
    </tr>
  </tbody>
</table>