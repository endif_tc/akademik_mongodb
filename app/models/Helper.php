<?php
/**
 * Created by PhpStorm.
 * User: fendi
 * Date: 4/21/14
 * Time: 2:26 PM
 */

class Helper {
    public static function show_array($array=array()){
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

    public static function validatorMessageStr(\Illuminate\Validation\Validator $v){
        $errorString="";
        foreach($v->messages()->all() as $error){
            $errorString.=$error."<br>";
        }
        return $errorString;
    }
    public static function validatorMessageStrFromArray(array $v){
        $errorString="";
        foreach($v as $error){
            $errorString.=$error."<br>";
        }
        return $errorString;
    }

    public static function randomArray($array,$limited=null)
    {
        if(!is_array($array)){
            $arrayTemp=$array;
            $array=array();
            for($i=0;$i<count($arrayTemp);$i++){
                $array[$i]=$i;
            }
        }
        if(count($array)==0) return array();
        if($limited!=null){
            $rand_keys = array_rand($array, $limited);
            if($limited==1){
                if(isset($arrayTemp)){
                    return $arrayTemp[$rand_keys];
                }
                return $result[]=$array[$rand_keys];
            }
        }else{
            $rand_keys = array_rand($array);
        }
        $result=array();
        if(!is_array($rand_keys)) return $array;
        if(isset($arrayTemp)){
            $arraySelected=null;
            foreach($rand_keys as $key){
                $arraySelected[$key]=$arrayTemp[$key];
            }
            return $arraySelected;
        }else{
            foreach($rand_keys as $key){
                $result[]=$array[$key];
            }
        }
        return $result;
    }
    public static function encrypt($id,$minLength=6,$isJustNumber=false){
        $dateTime=new DateTime('now');
        if(strlen($id)){
            $id='0'.$id;
        }
        if($id>100){
            $id=$id % 100;
        }
        if($isJustNumber){
            $keyList=array('5 6 4 7 3 8 2 9 1 0');
        }else{
            $keyList=array(
                    '7 6 S 5 A 1 R 0 2 8',
                    '5 L T I A M 1 N 4 H',
                    '8 J K L Y T 7 9 K 2',
                    'Q W E R 1 L P U S G',
                    'O X C V 5 B D E F S',
                );
            $keyList=array($keyList[array_rand($keyList,1)]);
        }

        $id=substr($id.$dateTime->format('isHdmy'),0,$minLength);
        $key=$keyList[rand(0, 0)];

        $keySplit=  explode(' ', $key);
        $result="";
        for($i=0;$i<strlen($id);$i++){
            $substr=  substr($id, $i, 1);
            $result.=$keySplit[$substr];
        }
        return $result;
    }

    public static function sendSms($message,$destinationNumber){
        $message = urlencode($message);
        $phone_no=$destinationNumber;
        $phone_no=ltrim($phone_no,'0');
        // sms
        $config['raja_sms_key'] = 'c3bf80faf994fe75f5d111060f60b781'; // raja sms key
        $config['zenziva_userkey'] = 'dowc4l';
        $config['zenziva_paskey'] = 'songpop';


        $curlHandle = curl_init();
        $url="http://zenziva.net/apireguler/smsapi.php?userkey={$config['zenziva_userkey']}&passkey={$config["zenziva_paskey"]}&nohp='0'.{$phone_no}&pesan=$message";
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        // curl_setopt($curlHandle, CURLOPT_PROXY, 'localhost:8888');
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        $hasil = curl_exec($curlHandle);
        curl_close($curlHandle);
        // Helper::show_array($hasil);
    }

    public static function convert_datetime_timezone($datetime,$timezoneFrom,$timezoneTo){
        $carbon=\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$datetime,$timezoneFrom);
        $carbon->setTimezone($timezoneTo);
        return $carbon->toDateTimeString();
    }

    public static function getMonthDropdownModel()
    {
        return array(
            null=>'- Month -',
            1=>'Januari',
            2=>'Februari',
            3=>'Maret',
            4=>'April',
            5=>'Mei',
            6=>'Juni',
            7=>'Juli',
            8=>'Agustus',
            9=>'September',
            10=>'Oktober',
            11=>'November',
            12=>'Desember',
        );
    }
    public static function getYearDropdownModel()
    {
        $result[null]='- Year -';
        for($i=date('Y');$i<date('Y')+10;$i++){
            $result[$i]=$i;
        }
        return $result;
    }
    public static function currentController($index=null)
    {
        $routeArray = Str::parseCallback(Route::currentRouteAction(), null);
        if($index){
            return isset($routeArray[$index])?$routeArray[$index]:null;
        }
        if (last($routeArray) != null) {
            // Remove 'controller' from the controller name.
            $controller = str_replace('Controller', '', class_basename(head($routeArray)));

            return $controller;
        }

        return 'closure';
    }
    public static function paypalUrl($url){
        return "https://api-3t.sandbox.paypal.com/".$url;
//        return "https://api-3t.paypal.com/".$url; ==>for live
    }

    public static function getPaypalSignature(){
        // return array(
        //     "USER"=>"fenditricahyono-facilitator_api1.gmail.com",
        //     "PWD"=>"1396329590",
        //     "SIGNATURE"=>"Ag8K4W.rhekgHJ1HMzjWWJYN6c7tAo3Y8IqKnpIy2tBLClkbVd4cLD.Q",
        //     "VERSION"=>"86",
        // );
        return array(
            "USER"=>"fenditricahyono_api1.gmail.com",
            "PWD"=>"U7NSU43V4FE2P4YM",
            "SIGNATURE"=>"AyshxflJjkluz7lxAFGxW0ufPYTKAj6Ngg25DpLhkuWy-hCtNBIgzlfe",
            "VERSION"=>"86",
        );
    }
    static $currency;
    public static function currencyConverter($val,$from="IDR",$to="USD")
    {
        if($from==$to)
            return $val;
        if(isset(Helper::$currency[$from][$to])){
            $content=Helper::$currency[$from][$to];
        }else{
            $simpleHTML = new \SimpleHtmlDom();
            $html = $simpleHTML->file_get_html("https://www.google.com/finance/converter?a=1&from=$from&to=$to&meta=ei%3DoIthU8jMMojckQX-iAE");
            foreach ($html->find("span.bld") as $a) {
                $content = str_replace('<span class=bld>','',$a);
                $content = str_replace('</span>','',$content);
                Helper::$currency[$from][$to]=$content=str_replace(' '.$to,'',$content);
            }
        }
        return $content*$val;
    }

    
    public static function rupiah($nominal,$withRp=true){
        $rupiah = number_format($nominal, 0, ",", ".");
        if($withRp)
            $rupiah = "Rp " . $rupiah . ",-";
        return $rupiah;
    }
    public static function deleteUpload($file){
        if($file=='' || $file==null) return false;
        if(file_exists('upload/'.$file)){
            unlink('upload/'.$file);
        }
        if(file_exists('upload/thumbnail/'.$file)){
            unlink('upload/thumbnail/'.$file);
        }
        if(file_exists('upload/medium/'.$file)){
            unlink('upload/medium/'.$file);
        }
        if(file_exists('upload/small/'.$file)){
            unlink('upload/small/'.$file);
        }
        if(file_exists('upload/large/'.$file)){
            unlink('upload/large/'.$file);
        }
    }


    /*
        Fungsi addTime($nama_kolom_sql,$penambahan_waktu)
        Makna: memberikan fungsi sql addtime yang berarti penambahan waktu pada kolom yang dikehendaki
        Contoh pemanggilan addTime('created_at','1 02:00:00')
        Kembalian/return : date(addtime(created_at,'1 02:00:00'))
        Penggunaan : select date(addtime(now,'0 01:00:00')) -> artinya menampilkan waktu satu jam dari sekarang
    */
    public static function addTime($nama_kolom_sql, $penambahan_waktu,$negatif=false){
        if($negatif){
            return 'date(subtime('.$nama_kolom_sql.','.'"'.$penambahan_waktu.'"))';
        }  else {
            return 'date(addtime('.$nama_kolom_sql.','.'"'.$penambahan_waktu.'"))';
        }
    }

    /*
        fungsi indonesianTime(), pengembangan addTime dengan menyesuaikan waktu indonesia bagian barat
    */
    public static function indonesianTime($nama_kolom_sql){
        return self::addtime($nama_kolom_sql,'0 07:00:00');
    }

    // Menjadikan timestamp utc ke time stamp indonesia bagian barat
    // Misalkan time stamp awal : 2014-01-03 04:00:00
    // Maka time stamp hasil atau return : 2014-01-02 21:00:00
    public static function setTimeToIndonesia($timestamp){
        return $timestamp + (7*60*60);
    }
    public static function getUrl(array $urlArray){
        return '?'.http_build_query($urlArray);
    }
    public static function http_response($url, $status = null, $wait = 3)
    {
        $time = microtime(true);
        $expire = $time + $wait;

        // we fork the process so we don't have to wait for a timeout

            // we are the parent
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $head = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            return $httpCode;
    }
    public static function generateGetParam($array=array()){
        return http_build_query(array($array => Input::get($array)));
        // if(!is_array($array)){
        //     $array=\Input::get($array);
        // }
        $result=urlencode(serialize($array));
        // echo $result
        return $result;
    }
    public static function dropDownModel($array,$key,$label,$result=null){
        if($result==null)
            $result=array();
        elseif(!is_array($result)){
            $result=array(''=>$result);
        }
        // Helper::show_array($array);exit;
        foreach($array as $a){
            // Helper::show_array($a->toArray());
            // echo $a->{$key}.' '.$a->{$label};
            $result[$a->{$key}.'']=$a->{$label};
        }
        // Helper::show_array($result);
        // exit;
        return $result;
    }
    public static function jenisKelaminList(){
        return array(''=>'- Jenis Kelamin -','L'=>'Laki-laki','P'=>'Permpuan');
    }
    public static function shortDate($date){
        $explode=explode('-', $date);
        return $explode[2].'/'.$explode[1];
    }
    public static function date2mysql($tgl) {
        if($tgl==null || $tgl=='') return null;
        $new = null;
        $tgl = explode("/", $tgl);
        if (empty($tgl[2]))
            return "";
        $new = "$tgl[2]-$tgl[1]-$tgl[0]";
        return $new;
    }
    public static function datefmysql($tanggal,$to_str=false){
        if($tanggal==null || $tanggal=='' || $tanggal=='0000-00-00') return null;
        $pecahTanggal = explode("-", $tanggal);
        $tanggal = $pecahTanggal[2];
        $bulan   = $pecahTanggal[1];
        $tahun   = $pecahTanggal[0];
        if($to_str)
            return Helper::indo_tgl("$tanggal/$bulan/$tahun",'/');
        return "$tanggal/$bulan/$tahun";
    }
    public static function indo_tgl($tgl, $type=null) {
        if ($type == null) {
            $type = "/";
        }
        $mo="";
        $tgl = explode($type, $tgl);
        if ($tgl[1] == '01')
            $mo = "Jan";
        if ($tgl[1] == '02')
            $mo = "Feb";
        if ($tgl[1] == '03')
            $mo = "Mar";
        if ($tgl[1] == '04')
            $mo = "Apr";
        if ($tgl[1] == '05')
            $mo = "Mei";
        if ($tgl[1] == '06')
            $mo = "Jun";
        if ($tgl[1] == '07')
            $mo = "Jul";
        if ($tgl[1] == '08')
            $mo = "Ags";
        if ($tgl[1] == '09')
            $mo = "Sep";
        if ($tgl[1] == '10')
            $mo = "Okt";
        if ($tgl[1] == '11')
            $mo = "Nov";
        if ($tgl[1] == '12')
            $mo = "Des";
        $new = "$tgl[0] $mo $tgl[2]";
        return $new;
    }
    public static function jam($jam){
        return substr($jam, 0,5);
    }
    public static function getDateInterval($startDate,$endDate){
        $tanggal=DB::select('select * from (select aDate from (
              select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
              (select 0 as a union all select 1 union all select 2 union all select 3
               union all select 4 union all select 5 union all select 6 union all
               select 7 union all select 8 union all select 9) a, 
              (select 0 as a union all select 1 union all select 2 union all select 3
               union all select 4 union all select 5 union all select 6 union all
               select 7 union all select 8 union all select 9) b,
              (select 0 as a union all select 1 union all select 2 union all select 3
               union all select 4 union all select 5 union all select 6 union all
               select 7 union all select 8 union all select 9) c,
              (select 0 as a union all select 1 union all select 2 union all select 3
               union all select 4 union all select 5 union all select 6 union all
               select 7 union all select 8 union all select 9) d,
              (select @minDate := "'.$startDate.'", @maxDate := "'.$endDate.'") e
            ) f 
            where aDate between @minDate and @maxDate) mydate order by aDate asc');
        $result=array();
        foreach($tanggal as $t){
            $result[$t->aDate]=$t->aDate;
        }
        return $result;
    }
    public static function getMonthInterval($startMonth,$startYear,$endMonth,$endYear){
        if(($startYear<$endYear) || ($startYear==$endYear && $startYear<=$endYear)){
            $result["$startMonth/$startYear"]=array('month'=>$startMonth,'year'=>$startYear);
            
            while(!($startYear==$endYear && $startMonth==$endMonth)){
                $startMonth++;
                if($startMonth>12){
                    $startMonth=1;
                    $startYear++;
                }
                $result["$startMonth/$startYear"]=array('month'=>$startMonth,'year'=>$startYear);
            }
            return $result;
        }else return array();
    }
    public static function shortYear($year){
        return substr($year, 2);
    }
}
