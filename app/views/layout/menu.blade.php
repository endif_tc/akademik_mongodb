<?php
$menuList=array(
        array('label'=>'Dashboard','role'=>null,'href'=>URL::to('home'),'selected'=>Helper::currentController() == 'Home','icon'=>'fa fa-home',),
        array('label'=>'Jurusan','role'=>'view_jurusan','href'=>URL::to('jurusan'),'selected'=>Helper::currentController() == 'Jurusan','icon'=>'fa fa-globe',),
        array('label'=>'Mahasiswa','role'=>'view_jurusan','href'=>URL::to('mahasiswa'),'selected'=>Helper::currentController() == 'Mahasiswa','icon'=>'fa fa-globe',),
);

?>
<ul class="nav nav-pills nav-stacked nav-bracket NonPrintable">
        @foreach($menuList as $menu)
                        <li class="{{ $menu['selected']?'active':''}} "><a href="{{ $menu['href'] }}"><i class="{{ $menu['icon'] }}"></i> <span>{{ $menu['label'] }}</span></a></li>                
                @if(isset($menu['subMenu']))
                        <li class="{{ $menu['selected']?'active':''}} {{ isset($menu['subMenu'])?"nav-parent nav-hover":""}}">
                                <a><i class="{{ $menu['icon'] }}"></i> <span>{{ $menu['label'] }}</span></a>
                                <ul class="children" {{ $menu['selected']?'style="display: block"':''}}>
                                @foreach($menu['subMenu'] as $sub)
                                    <li class="{{ $sub['selected']?'active':''}}"><a href="{{ $sub['href']}}"><i class="{{ $sub['icon'] }}"></i> {{ $sub['label'] }}</a></li>
                                @endforeach
                                </ul>
                        </li>                
                @endif
        @endforeach
</ul>