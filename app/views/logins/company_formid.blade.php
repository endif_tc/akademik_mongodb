    <div class="" id='information-form' style='display:none'>
        <h3 class="nomargin">Informasi Perusahaan</h3>
        <p class="mt5 mb20">Sudah menjadi member? <a href="{{ URL::to("idn/login") }}">Sign In</a></p>
        <div class="mb10">
            <label class="control-label">Nama Perusahaan</label>
            {{ Form::text(
                "company_name",
                Input::old("company_name"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("company_name")}} </span>
        </div>

        <div class="mb10">
            <label class="control-label">Website Perusahaan (Jika ada)</label>
            {{ Form::text(
                "company_website",
                Input::old("company_website"),
                array(
                    "class" => "form-control url",
                    "placeholder" => "Click to assign",
                    "value" => "",
                    "onClick" =>"if(this.value=='')this.value='http://'",
                    "onBlur"  =>"if(this.value=='http://')this.value=''"
                ))
            }} 
            <span class="err"> {{$errors->first("company_website")}} </span>
        </div>
        
        <div class="mb10">
            <!-- <label class="control-label">Country</label>
            {{ Form::text(
                "country",
                Input::old("country"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("country")}} </span> -->

            <label class="control-label">Negara</label>
            <select name="country" class="form-control required">
                <option value=''>- Countries -</option>
                <?php 
                $category=Countries::orderBy('country_name')->get();
                ?>
                @foreach($category as $country)
                    <option value='{{ $country->id }}' {{ $country->id==Input::old("country_id")?"selected":""}}>
                        {{ $country->country_name }}
                    </option>    
                @endforeach
            </select>
            <span class="err"> {{$errors->first("industry_id")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Nomor Telepon Perusahaan</label>
            {{ Form::text(
                "company_phone_number",
                Input::old("company_phone_number"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("company_phone_number")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Alamat Perusahaan</label>
            {{ Form::text(
                "company_office_address",
                Input::old("company_office_address"),
                array(
                    "class" => "form-control required"
                ))
            }}
            <span class="err"> {{$errors->first("company_office_address")}} </span>
        </div>
        
        <div class="mb10">
            <label class="control-label">Industri</label>
            <select name="industry_id" class="form-control required">
                <option value=''>- Industry -</option>
                <?php 
                $category=IndustryCategory::orderBy('industry_name')->get();
                ?>
                @foreach($category as $industry)
                    <option value='{{ $industry->id }}' {{ $industry->id==Input::old("industry_id")?"selected":""}}>
                        {{ $industry->industry_name }}
                    </option>    
                @endforeach
            </select>
            <span class="err"> {{$errors->first("industry_id")}} </span>
        </div>

        <div class="mb10">
            <label class="control-label">Jumlah Pegawai</label>
            <select name="employee_count_category_id" class="form-control required">
                <option value=''>- No. Employees -</option>
                @foreach(EmployeeCountCategory::all() as $a)
                    <option value='{{ $a->id }}' {{ $a->id==Input::old("employee_count_category_id")?"selected":""}} >
                        {{ $a->em_count }}
                    </option>    
                @endforeach
            </select>
            <span class="err"> {{$errors->first("employee_count_category_id")}} </span>
        </div>

        <div class="mb10">
            <label class="control-label">Budget Survey Bulanan</label>
            <select name="monthly_budget_category_id" class="form-control required">
                <option value=''>- Monthly Budget -</option>
                @foreach(MonthlyBudgetCategory::all() as $a)
                    <option value='{{ $a->id }}' {{ $a->id==Input::old("monthly_budget_category_id")?"selected":""}}>
                        {{ $a->budget_monthly }}
                    </option>    
                @endforeach
            </select>
            <span class="err"> {{$errors->first("monthly_budget_category_id")}} </span>
        </div>

        <br />
        <button class="btn btn-primary btn-block">Sign Up</button>  
</div>