;App=(function(){
    function flashMessage(type, message, target) {
        $target = target ? $(target) : $('#alert-container');
        $target.prepend('<div class="alert alert-'+type+'"><button class="close" data-dismiss="alert">&times;</button>'+message+'</div>');
        setTimeout(function(){
            $target.find('.alert').fadeOut(4000,function(){
                $(this).find('.close').click();
            });
        },2000);
      }

    return {
        flashMessage:flashMessage
    }
}());

(function($){//
  $.fn.fixedFirstColumnLaporan=function(options){
    var settings=$.extend({
      columns:[{index:0,label:"Rute",width:200}],
      complete:null
    },options);

    return this.each(function(i,val){
      var outerContainer=['<div id="outer-container-laporan">',
      '<div id="inner-container-laporan"></div>',
      ,'</div>'];
      $(this).addClass('table-container');
      $(this).append(outerContainer.join(''));
      var elem=$(this);
      var left=5;
      $.each(settings.columns,function(i,data){
        elem.find('table>thead').find('th').eq(data.index).removeAttr('rowspan').attr('style','border-top: 1px solid #dddddd;border-bottom:0;width:'+data.width+'px;left:'+left+'px').addClass('fixed-column').html('&nbsp;');
        elem.find('table>thead').find('tr').eq(1).prepend('<th style="border-top:0;width:'+data.width+'px;left:'+left+'px" class="fixed-column">'+data.label+'</th>');
        elem.find('table td:nth-child('+(parseInt(data.index)+1)+')').addClass('fixed-column').css('width',data.width+'px').css('left',left+'px');
        elem.find('table>tfoot').find('tr').eq(0).prepend('<td style="width:'+data.width+'px;left:'+left+'px" class="fixed-column">&nbsp;</th>');
        left+=parseInt(data.width);
      });
      $('#inner-container-laporan').css('margin-left',(left-1)+'px');
      $('#outer-container-laporan').css('right',left+'px');
      $('#inner-container-laporan').css('padding-top','1px');
      $(this).find('table').appendTo('#inner-container-laporan');
      $(this).css('height',parseInt($(this).find('table').css('height'))+250+'px');

      if($.isFunction(settings.complete)){
        settings.complete.call(this);
      }
    });
  };
}(jQuery));//
