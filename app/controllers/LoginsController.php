<?php

class LoginsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /logins
	 *
	 * @return Response
	 */
	public function index($user_type=null)
	{
        if(Auth::user()){
            return Redirect::to('home');
        }
        $user=new User();
        if($_POST){
            $user->setRawAttributes($_POST['user']);
            if (Auth::attempt(array('username' => $user->username, 'password'=>$user->password)))
            {
                return Redirect::intended('home');
            } 
            else 
            {
                $cekUser = \User::where('username', $user->username)->first();
                $exist = false;
                if($cekUser && $user->active == 'no') {
                     $exist = true;
                }
                
                $this->message($exist,'Your account has not been activated. Check your email for the activation link.','Login failed.');
            }
        }
		return $this->renderPartial('logins.index',array('user'=>$user,'user_type'=>$user_type));
	}
    public function indexid($user_type=null)
    {
        if(Auth::user()){
            return Redirect::to('home');
        }
        $user=new User();
        if($_POST){
            $user->setRawAttributes($_POST['user']);
            if (Auth::attempt(array('username' => $user->username, 'password'=>$user->password, 'active' => 'yes')))
            {
                return Redirect::intended('home');
            } 
            else 
            {
                $user = $user->where('username', $user->username)->first();
                $exist = false;
                if($user && $user->active == 'no') {
                     $exist = true;
                }
                $this->message($exist,'Akun anda belum aktif. Cek email anda untuk link aktifasi.','Login gagal.');
            }
        }
        return $this->renderPartial('logins.indexid',array('user'=>$user,'user_type'=>$user_type));
    }

   public function logout(){
            Auth::logout();
            return Redirect::to("login");
    }

    public function getRegister($type='company')
    {
        $user=new User();
        return $this->renderPartial('logins.register',array('user'=>$user,'type'=>$type));
    }

    public function getRegisterid($type='company')
    {
        $user=new User();
        return $this->renderPartial('logins.registerid',array('user'=>$user,'type'=>$type));
    }

    public function postRegister()
    {
        $validator = Validator::make($_POST, array(
            "username"              => "required|alpha_dash|unique:user,username",
            "first_name"              => "required",
            "last_name"              => "required",
            "email"                 => "required|email|unique:user,email",
            "password"              => "required|min:6",
            "retype_password" => "same:password",
        ));
        // Helper::show_array($validator->messages());
        // Helper::show_array($_POST);exit;
        $message="";
        if($validator->passes())
        {
            $key = Hash::make(trim($_POST['password']).trim($_POST['username']).'G1T');
            $user = new User;
            $user->first_name = trim($_POST['first_name']);
            $user->last_name = trim($_POST['last_name']);
            $user->username = trim($_POST['username']);
            $user->email = trim($_POST['email']);
            $user->password = Hash::make(trim($_POST['password']));
            $user->verify_key = $key;
            $user->role = 'user';
            $user->active = 'no';
            $user->type=trim($_POST['type']);
            $salesPartner=null;

            if(isset($_POST['sales_code']) && $_POST['sales_code']!=""){
                $salesPartner=\SalesPartner::where('code','=',$_POST['sales_code'])->first();
                if($salesPartner!=null){
                    $user->sales_partner_id=$salesPartner->id;
                }else
                     $message.="Sales Partner Code Not Found";
            }
            if($user->save()) {
                if($user->type=='student'){
                    $student=new StudentProfile();
                    $student->user_id     =$user->id;
                    $student->university    =trim($_POST['university']);
                    $student->website       =trim($_POST['university_website']);
                    $student->location      =trim($_POST['university_location']);
                    $student->card_number   =trim($_POST['card_number']);
                    $student->major         =trim($_POST['university_major']);
                    $student->save();

                }elseif($user->type=='company'){
                    $company=new CompanyProfile();
                    $company->name      =trim($_POST['company_name']);
                    $company->website   =trim($_POST['company_website']);
                    $company->country   =trim($_POST['country']);
                    $company->phone_number      =trim($_POST['company_phone_number']);
                    $company->office_address    =trim($_POST['company_office_address']);
                    $company->industry_category_id  =trim($_POST['industry_id']);
                    $company->employee_count_category_id    =trim($_POST['employee_count_category_id']);
                    $company->monthly_budget_category_id    =trim($_POST['monthly_budget_category_id']);
                    $company->user_id =$user->id;
                    $company->save();
                }
                $data = array(
                    'name' => trim($_POST['first_name']).' '.trim($_POST['last_name']),
                    'url' =>  action('LoginsController@getVerify', array('email' => trim($_POST['email']), 'key' => $key)),
                    'email' => trim($_POST['email']) );
                Mail::send('logins.verify', $data, function($message)
                {
                    $message->to(trim($_POST['email']), trim($_POST['first_name']).' '.trim($_POST['last_name']))->subject('Verify your Email');

                });
                return Redirect::to('login')
                    ->with('success', 'Your registration has been successful. Check your email for the activation link.<br>'.$message);
            }
            return Redirect::route('register')
            ->with('failed', 'Failed to create new user!')
            ->withInput();
        }
        else
        {
            return Redirect::route('register')
                ->with('failed', 'Failed to create new user!')
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function postRegisterid()
    {
        $validator = Validator::make($_POST, array(
            "username"              => "required|alpha_dash|unique:user,username",
            "first_name"              => "required",
            "last_name"              => "required",
            "email"                 => "required|email|unique:user,email",
            "password"              => "required|min:6",
            "retype_password" => "same:password",
        ));
        // Helper::show_array($validator->messages());
        // Helper::show_array($_POST);exit;
        if($validator->passes())
        {
            $key = Hash::make(trim($_POST['password']).trim($_POST['username']).'G1T');
            $user = new User;
            $user->first_name = trim($_POST['first_name']);
            $user->last_name = trim($_POST['last_name']);
            $user->username = trim($_POST['username']);
            $user->email = trim($_POST['email']);
            $user->password = Hash::make(trim($_POST['password']));
            $user->verify_key = $key;
            $user->role = 'user';
            $user->active = 'no';
            $user->type=trim($_POST['type']);

            if($user->save()) {
                if($user->type=='student'){
                    $student=new StudentProfile();
                    $student->user_id     =$user->id;
                    $student->university    =trim($_POST['university']);
                    $student->website       =trim($_POST['university_website']);
                    $student->location      =trim($_POST['university_location']);
                    $student->card_number   =trim($_POST['card_number']);
                    $student->major         =trim($_POST['university_major']);
                    $student->save();

                }elseif($user->type=='company'){
                    $company=new CompanyProfile();
                    $company->name      =trim($_POST['company_name']);
                    $company->website   =trim($_POST['company_website']);
                    $company->country   =trim($_POST['country']);
                    $company->phone_number      =trim($_POST['company_phone_number']);
                    $company->office_address    =trim($_POST['company_office_address']);
                    $company->industry_category_id  =trim($_POST['industry_id']);
                    $company->employee_count_category_id    =trim($_POST['employee_count_category_id']);
                    $company->monthly_budget_category_id    =trim($_POST['monthly_budget_category_id']);
                    $company->user_id =$user->id;
                    $company->save();
                }
                $data = array(
                    'name' => trim($_POST['first_name']).' '.trim($_POST['last_name']),
                    'url' =>  action('LoginsController@getVerifyid', array('email' => trim($_POST['email']), 'key' => $key)),
                    'email' => trim($_POST['email']) );
                Mail::send('logins.verify', $data, function($message)
                {
                    $message->to(trim($_POST['email']), trim($_POST['first_name']).' '.trim($_POST['last_name']))->subject('Verify your Email');

                });
                return Redirect::to('idn/login')
                    ->with('sukses', 'Registrasi anda telah berhasil. Cek email anda untuk link aktifasi.');
            }
            exit;
            return Redirect::route('idn/register')
            ->with('gagal', 'Gagal membuat user baru!')
            ->withInput();
        }
        else
        {
            return Redirect::route('idn/register')
                ->with('gagal', 'Gagal membuat user baru!')
                ->withErrors($validator)
                ->withInput();
        }
    }

    public function getVerify()
    {
        $param = array(
                'email' => Input::get('email'),
                'key' => Input::get('key')
            );
        $validator = Validator::make($param, array(
            "email"                 => "required|email",
            "key"              => "required"
        ));

        if ($validator->passes())
        {
            $user = User::where('email', Input::get('email'))
                ->where('verify_key', Input::get('key'))
                ->first();

            if($user) {
                $user->active = 'yes';
                $active = $user->save();
                if($active) {
                    return Redirect::to('login')->with('success', 'Your account has been activated.');
                }
            }
            return Redirect::to('login')->with('failed', 'Verification failed.');
        }
        else 
        {
            return Redirect::to('login')->with('failed', 'Email or Key invalid.');
        }

    }

    public function getVerifyid()
    {
        $param = array(
                'email' => Input::get('email'),
                'key' => Input::get('key')
            );
        $validator = Validator::make($param, array(
            "email"                 => "required|email",
            "key"              => "required"
        ));

        if ($validator->passes())
        {
            $user = User::where('email', Input::get('email'))
                ->where('verify_key', Input::get('key'))
                ->first();

            if($user) {
                $user->active = 'yes';
                $active = $user->save();
                if($active) {
                    return Redirect::to('idn/login')->with('sukses', 'Akun anda telah aktif.');
                }
            }
            return Redirect::to('idn/login')->with('gagal', 'Gagal verifikasi.');
        }
        else 
        {
            return Redirect::to('idn/login')->with('gagal', 'Email atau Key tidak valid.');
        }

    }

    public function getForgotPassword()
    {
        $user=new User();
        return $this->renderPartial('logins.forgot_password',array('user'=>$user));
    }

    public function getForgotPasswordid()
    {
        $user=new User();
        return $this->renderPartial('logins.forgot_passwordid',array('user'=>$user));
    }

    public function postForgotPassword()
    {
        $validator = Validator::make($_POST, array(
            "email"                 => "required|email",
        ));
        if($validator->fails())
        {
            return Redirect::route('forgotPassword')
                ->withErrors($validator)
                ->withInput();
        }
        $user = new User;
        $user->setRawAttributes($_POST);
        $user = $user->where('email', $user->email)->first();
        $exist = false;
        if($user && $user->email == $user->email) {
            $user->verify_key = Hash::make($user->password.$user->username.$user->email);
            if($user->save()) {
                $data = array(
                    'name' => $user->first_name.' '.$user->last_name,
                    'url' =>  action('LoginsController@getResetPassword', array('email' => $user->email, 'key' => $user->verify_key)),
                    'email' => $user->email );

                Mail::send('logins.email_reset', $data, function($message) use ($user)
                {
                    $message->to($user->email, $user->first_name.' '.$user->last_name)->subject('Request Reset Password');

                });
                $exist = true;
            }
        }
        $this->message($exist,'Check your email for the reset password link.','Request reset failed.');
        return $this->renderPartial('logins.forgot_password',array('user'=>$user));
    }

    public function postForgotPasswordid()
    {
        $validator = Validator::make($_POST, array(
            "email"                 => "required|email",
        ));
        if($validator->fails())
        {
            return Redirect::route('idn/forgotPassword')
                ->withErrors($validator)
                ->withInput();
        }
        $user = new User;
        $user->setRawAttributes($_POST);
        $user = $user->where('email', $user->email)->first();
        $exist = false;
        if($user && $user->email == $user->email) {
            $user->verify_key = Hash::make($user->password.$user->username.$user->email);
            if($user->save()) {
                $data = array(
                    'name' => $user->first_name.' '.$user->last_name,
                    'url' =>  action('LoginsController@getResetPasswordid', array('email' => $user->email, 'key' => $user->verify_key)),
                    'email' => $user->email );

                Mail::send('logins.email_resetid', $data, function($message) use ($user)
                {
                    $message->to($user->email, $user->first_name.' '.$user->last_name)->subject('Request Reset Password');

                });
                $exist = true;
            }
        }
        $this->message($exist,'Cek email anda untuk link reset password.','Permintaan reset gagal.');
        return $this->renderPartial('logins.forgot_passwordid',array('user'=>$user));
    }

    public function getResetPassword()
    {
        $param = array(
                'email' => Input::get('email'),
                'key' => Input::get('key')
        );
        $validator = Validator::make($param, array(
            "email"                 => "required|email",
            "key"              => "required"
        ));

        if ($validator->passes())
        {
            $user = User::where('email', Input::get('email'))
                ->where('verify_key', Input::get('key'))
                ->first();

            if($user) {
                return $this->renderPartial('logins.reset',array('user'=>$user));
            }
            return Redirect::to('forgotPassword')->with('failed', 'Reset password failed.');
        }
        else 
        {
            return Redirect::to('forgotPassword')->with('failed', 'Email or key invalid.');
        }
    }

    public function postResetPassword()
    {
        $validator = Validator::make($_POST, array(
            "password"              => "required|min:6",
            "email"              => "required|email",
            "verify_key"              => "required",
            "retype_password" => "same:password",
        ));
        if($validator->fails())
        {
            return Redirect::route('resetPassword', array('email' => $_POST['email'], 'key' => $_POST['verify_key']))
                ->withErrors($validator)
                ->withInput();
        }
        $user = new User;
        $user->setRawAttributes($_POST);
        $user = $user->where('email', $user->email)->where('verify_key', $user->verify_key)->first();
        $success = false;
        if($user && $user->email == $user->email) {
            $user->password = Hash::make($user->password);
            if($user->save()) {
                $success = true;
            }
        }
        $this->message($success,'Your password has been changed.','Change password failed.');
        return $this->renderPartial('logins.index',array('user'=>$user));
    }
    public function validate($type){
        if($type=='username'){
            $result=\User::where('username','=',$_GET['username'])->get();
        }elseif($type=='salesCode'){
            $result=\SalesPartner::where('code','=',$_GET['sales_code'])->get();    
            return count($result)>0?"true":"false";
        }else{
            $result=\User::where('email','=',$_GET['email'])->get();
        }
        return count($result)<=0?"true":"false";
    }
}