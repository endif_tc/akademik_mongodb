@extends('layout/row')
@section('content')
<?php //echo Form::pass?>
        {{ Form::model($jurusan,array('route'=>array($jurusan->isNewRecord()?'jurusan.store':'jurusan.update',$jurusan->_id),'files'=>'true',
        'class'=>'form-horizontal form-bordered','jurusan'=>'jurusan-form','method'=>$jurusan->isNewRecord()?'POST':'PUT')) }}
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="#" class="panel-close" data-dismiss="modal">&times;</a>
            </div>
            <h4 class="panel-title">{{ $jurusan->isNewRecord()?"Tambah":"Edit" }} Jurusan</h4>
        </div>
        <div class="panel-body panel-body-nopadding" style="display: block;">
            <div class="form-group">
                {{ Form::label('jurusan[kode_jurusan]','Kode',array('class'=>'col-sm-3 control-label')) }}
                <div class="col-sm-2">
                    {{ Form::text('jurusan[kode_jurusan]',$jurusan->kode_jurusan,array('class'=>'col-sm-4  required form-control','placeholder'=>'Kode Jurusan')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('jurusan[nama]','Jurusan',array('class'=>'col-sm-3 control-label')) }}
                <div class="col-sm-5">
                    {{ Form::text('jurusan[nama]',$jurusan->nama,array('class'=>'col-sm-8  required form-control','placeholder'=>'Jurusan')) }}
                </div>
            </div>
        </div>
        <div class="panel-footer" style="display: block;">
            {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}
            {{ Form::reset('Reset',array('class'=>'btn btn-default')) }}
        </div>
        {{ Form::close() }}
<script type="text/javascript">
    $(document).ready(function(){
        $('form[jurusan=jurusan-form]').validate();
    });
</script>
@stop