@extends('layout/row')
@section('content')
<?php //echo Form::pass?>
        {{ Form::model($mahasiswa,array('route'=>array($mahasiswa->isNewRecord()?'mahasiswa.store':'mahasiswa.update',$mahasiswa->_id),'files'=>'true',
        'class'=>'form-horizontal form-bordered','mahasiswa'=>'mahasiswa-form','method'=>$mahasiswa->isNewRecord()?'POST':'PUT')) }}
        <div class="panel-heading">
            <div class="panel-btns">
                <a href="#" class="panel-close" data-dismiss="modal">&times;</a>
            </div>
            <h4 class="panel-title">{{ $mahasiswa->isNewRecord()?"Tambah":"Edit" }} Mahasiswa</h4>
        </div>
        <div class="panel-body panel-body-nopadding" style="display: block;">
            <div class="form-group">
                {{ Form::label('mahasiswa[nim]','NIM',array('class'=>'col-sm-3 control-label')) }}
                <div class="col-sm-2">
                    {{ Form::text('mahasiswa[nim]',$mahasiswa->nim,array('class'=>'col-sm-4  required form-control','placeholder'=>'Kode Mahasiswa')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('mahasiswa[nama]','Nama',array('class'=>'col-sm-3 control-label')) }}
                <div class="col-sm-5">
                    {{ Form::text('mahasiswa[nama]',$mahasiswa->nama,array('class'=>'col-sm-8  required form-control','placeholder'=>'Mahasiswa')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('mahasiswa[id_jurusan]','Kota',array('class'=>'col-sm-3 control-label')) }}
                <div class="col-sm-5">
                    {{ Form::select('mahasiswa[id_jurusan]', Helper::dropDownModel($jurusanList,'_id','nama',array(''=>'- Pilih Jurusan -')), $mahasiswa->id_jurusan,array('class'=>'form-control mb15')); }}
                </div>
            </div>
        </div>
        <div class="panel-footer" style="display: block;">
            {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}
            {{ Form::reset('Reset',array('class'=>'btn btn-default')) }}
        </div>
        {{ Form::close() }}
<script type="text/javascript">
    $(document).ready(function(){
        $('form[mahasiswa=mahasiswa-form]').validate();
    });
</script>
@stop