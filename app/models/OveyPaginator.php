<?php
/**
 * Created by PhpStorm.
 * User: fendi
 * Date: 4/21/14
 * Time: 5:19 PM
 */

class OveyPaginator extends Illuminate\Pagination\Presenter {

    public function getActivePageWrapper($text)
    {
        return '<li class="active"><a href="#">'.$text.'</a></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><a href="#">'.$text.'</a></li>';
    }

    public function getPageLinkWrapper($url, $page,$rel=null)
    {
        return '<li><a href="'.$url.'">'.$page.'</a></li>';
    }

}