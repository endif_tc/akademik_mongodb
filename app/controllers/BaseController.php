<?php
class BaseController extends Controller {
	var $validator=null;
	var $pageheader;
    var $pageheader_desc;
    var $user;
    public function __construct(){
    	// parent::__construct();
        // Helper::show_array(Auth::user());exit;
    }
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	 public function render($view,$params=array()){
	 	\Debugbar::disable();
	 	// $a=new \Debugbar\JavascriptRenderer();
	 	// $a->setIncludeVendors();
	 	if(\Input::get('ajax')==1){
	 		return $this->renderPartial($view,$params);	
	 	}
	 	$params['user']=$this->user;
        return View::make('layout/master',array('params'=>$params,'view'=>$view,
            'validator'=>$this->validator, 'pageheader' => $this->pageheader,'user'=>$this->user));
    }
    public function renderPrint($view,$params=array()){
        \Debugbar::enable();
        $params['user']=$this->user;
        return View::make('layout/print',array('params'=>$params,'view'=>$view,
            'validator'=>$this->validator, 'pageheader' => $this->pageheader,'user'=>$this->user));
    }
    public function renderPartial($view,$params=array()){
        return View::make($view,$params);
    }

    public function message($isSuccess,$successMessage,$failedMessage){
        if($isSuccess){
            Session::flash('success',$successMessage);
        }else{
            Session::flash('failed',$failedMessage);
        }
    }

}
